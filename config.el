;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Garance Colomer"
      user-mail-address "gcolomer@student.42.fr"
      user-login-at-42 "gcolomer")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'garance)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type nil)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
;;
;;Size window

(set-face-attribute 'default nil :height 160)

(add-to-list 'initial-frame-alist '(fullscreen . maximized))

(setq projectile-project-search-path '("~/42"))

(map! "<f5>" #'revert-buffer)

(after! magit
  (magit-todos-mode))

(after! counsel
  (setq counsel-projectile-switch-project-action #'magit-status))

(set-file-templates! '("\\.c$" :trigger "__c" :mode c-mode)
                     '("\\.h$" :trigger "__h" :mode c-mode))

(defun c-mode-42-hook ()
  (setq-default tab-width 4
                indent-tabs-mode t
                c-default-style "linux"
                c-basic-offset 4)
  (c-set-offset 'substatement-open 0)
  (setq c-basic-offset 4 ;; Apparently buggy sometimes if you don't setq it as well
        whitespace-style '(tab-mark space-mark face tabs spaces)
        whitespace-display-mappings '((space-mark 32 [9251] [46])
                                      (tab-mark 9 [8594 9] [92 9]))))
(add-hook 'c-mode-hook 'c-mode-42-hook)

(flycheck-define-checker norminette
  "A checker for 42 norminette v2.

See URL https://github.com/42Paris/norminette"
  :command ("norminette" source)
  :error-patterns ((error line-start "Error: " (message) line-end)
                   (error line-start "Error (line " line "): " (message) line-end)
                   (error line-start "Error (line " line ", col " column "): " (message) line-end))
  :modes c-mode)
(add-to-list #'flycheck-checkers #'norminette t)

(add-hook! 'lsp-after-initialize-hook
  (run-hooks (intern (format "%s-lsp-hook" major-mode))))

(defun c-flyckeck-setup ()
  "Setup Flycheck checkers for C"
  (flycheck-add-next-checker 'lsp 'c/c++-cppcheck)
  (flycheck-add-next-checker 'lsp 'norminette)
  (print "Test"))

(add-hook 'c-mode-lsp-hook
          #'c-flyckeck-setup)
