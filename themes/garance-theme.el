(deftheme garance
  "Created 2021-11-18.")

(custom-theme-set-variables
 'garance
 )

(custom-theme-set-faces
 'garance
 '(default ((t (:inherit nil :extend nil :stipple nil :background "gray13" :foreground "thistle1" :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 161 :width normal :foundry "nil" :family "Monaco"))))
 '(cursor ((t (:background "aquamarine3"))))
 '(fixed-pitch ((t (:family "Monospace"))))
 '(variable-pitch ((((type w32)) (:foundry "outline" :family "Arial")) (t (:family "Sans Serif"))))
 '(escape-glyph ((t (:foreground "#8be9fd"))))
 '(homoglyph ((((background dark)) (:foreground "cyan")) (((type pc)) (:foreground "magenta")) (t (:foreground "brown"))))
 '(minibuffer-prompt ((t (:foreground "#bd93f9"))))
 '(highlight ((t (:background "gray18"))))
 '(region ((t (:extend t :background "gray20"))))
 '(shadow ((t (:foreground "#6272a4"))))
 '(secondary-selection ((t (:extend t :background "#565761"))))
 '(trailing-whitespace ((t (:background "#ff5555"))))
 '(font-lock-builtin-face ((t (:foreground "#ffb86c"))))
 '(font-lock-comment-delimiter-face ((t (:inherit (font-lock-comment-face)))))
 '(font-lock-comment-face ((t (:foreground "#6272a4"))))
 '(font-lock-constant-face ((t (:foreground "#8be9fd"))))
 '(font-lock-doc-face ((t (:foreground "#8995ba" :inherit (font-lock-comment-face)))))
 '(font-lock-function-name-face ((t (:foreground "#50fa7b"))))
 '(font-lock-keyword-face ((t (:foreground "#ff79c6"))))
 '(font-lock-negation-char-face ((t (:foreground "#bd93f9" :inherit (bold)))))
 '(font-lock-preprocessor-face ((t (:foreground "#bd93f9" :inherit (bold)))))
 '(font-lock-regexp-grouping-backslash ((t (:foreground "#bd93f9" :inherit (bold)))))
 '(font-lock-regexp-grouping-construct ((t (:foreground "#bd93f9" :inherit (bold)))))
 '(font-lock-string-face ((t (:foreground "#f1fa8c"))))
 '(font-lock-type-face ((t (:foreground "#bd93f9"))))
 '(font-lock-variable-name-face ((t (:foreground "#ffc9e8"))))
 '(font-lock-warning-face ((t (:inherit (warning)))))
 '(button ((t (:inherit (link)))))
 '(link ((t (:weight bold :underline (:color foreground-color :style line) :foreground "#bd93f9"))))
 '(link-visited ((t (:foreground "violet" :inherit (link)))))
 '(fringe ((t (:inherit default :foreground "black"))))
 '(header-line ((t (:inherit mode-line))))
 '(tooltip ((t (:foreground "#f8f8f2" :background "#1E2029"))))
 '(mode-line ((t (:box nil :background "#22232d"))))
 '(mode-line-buffer-id ((t (:weight bold))))
 '(mode-line-emphasis ((t (:foreground "#bd93f9"))))
 '(mode-line-highlight ((t (:inherit (highlight)))))
 '(mode-line-inactive ((t (:box nil :foreground "#6272a4" :background "#252631"))))
 '(isearch ((t (:weight bold :inherit (lazy-highlight)))))
 '(isearch-fail ((t (:weight bold :foreground "#1E2029" :background "#ff5555"))))
 '(lazy-highlight ((t (:weight bold :foreground "#f8f8f2" :background "#8466ae"))))
 '(match ((t (:weight bold :foreground "#50fa7b" :background "#1E2029"))))
 '(next-error ((t (:inherit (region)))))
 '(query-replace ((t (:inherit (isearch)))))
 '(window-divider ((t (:foreground "black" :width extra-expanded)))))

(provide-theme 'garance)
